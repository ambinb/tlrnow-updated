import { environment } from "../environments/environment";

export const appVariables = {
    currentModule: "",
    userToken: "",
    userId: "",
    userType: "",

}

export const userVars = {
    name:'',
    type:'',
    id:''
}

const BASE_URL = environment.api_url;
const U_B_URL = '/userapi/1/' 
export const urlsUser = {
    forgotPass: U_B_URL+'forgotpassword/'
}