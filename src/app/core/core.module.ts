import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService, UserService, JwtService } from './services';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ApiService,
    UserService,
    JwtService
  ]
})
export class CoreModule { }
