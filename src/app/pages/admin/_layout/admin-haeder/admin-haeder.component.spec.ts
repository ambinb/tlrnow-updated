import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHaederComponent } from './admin-haeder.component';

describe('AdminHaederComponent', () => {
  let component: AdminHaederComponent;
  let fixture: ComponentFixture<AdminHaederComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHaederComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHaederComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
