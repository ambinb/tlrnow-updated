import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminHaederComponent } from './_layout/admin-haeder/admin-haeder.component';
import { AdminSidebarComponent } from './_layout/admin-sidebar/admin-sidebar.component';
// import { CourseComponent } from '../course/course.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModuleModule
  ],
  declarations: [
    AdminDashboardComponent,
    AdminHaederComponent,
    AdminSidebarComponent
  ],
  exports:[
    AdminHaederComponent,
    AdminSidebarComponent
  ]
})
export class AdminModule { }
