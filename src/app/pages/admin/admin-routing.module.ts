import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { CourseComponent } from '../course/course.component';
// import { CourseDetailsComponent } from '../seeker/course-details/course-details.component';
import { ScheduleSessionsComponent } from '../schedule-sessions/schedule-sessions.component';
import { StartSessionsComponent } from '../start-sessions/start-sessions.component';
import { CourseDetailsComponent } from '../course-details/course-details.component';
import { AddCourseComponent } from '../add-course/add-course.component';

const routes: Routes = [
  {
    path: "",
    redirectTo:'dashboard',
    pathMatch:'full'
  },
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: AdminDashboardComponent
      },
      {
        path: 'course',
        component: CourseComponent
      },
      {
        path: 'course-details',
        component: CourseDetailsComponent
      },
      {
        path: 'add-course',
        component: AddCourseComponent
      },
      // {
      //   path: 'course-details',
      //   component: CourseDetailsComponent
      // },
      {
        path: 'schedule-sessions',
        component: ScheduleSessionsComponent
      },
      {
        path: 'start-sessions',
        component: StartSessionsComponent
      }
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
