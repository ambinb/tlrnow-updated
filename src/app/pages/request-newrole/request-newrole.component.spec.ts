import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestNewroleComponent } from './request-newrole.component';

describe('RequestNewroleComponent', () => {
  let component: RequestNewroleComponent;
  let fixture: ComponentFixture<RequestNewroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestNewroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestNewroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
