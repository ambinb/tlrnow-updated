import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-start-sessions',
  templateUrl: './start-sessions.component.html',
  styleUrls: ['./start-sessions.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class StartSessionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
