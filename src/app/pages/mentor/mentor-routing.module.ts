import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MentorDashboardComponent } from './mentor-dashboard/mentor-dashboard.component';
// import { CourseSelectComponent } from '../course-select/course-select.component';
import { Error404Component } from '../error-404/error-404.component';
import { CourseComponent } from '../course/course.component';
import { ScheduleSessionsComponent } from '../schedule-sessions/schedule-sessions.component';
import { StartSessionsComponent } from '../start-sessions/start-sessions.component';
import { CourseDetailsComponent } from '../course-details/course-details.component';
import { AddCourseComponent } from '../add-course/add-course.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: MentorDashboardComponent
      },
      {
        path: 'course',
        component: CourseComponent
      },
      {
        path: 'course-details',
        component: CourseDetailsComponent
      },
      {
        path: 'schedule-sessions',
        component: ScheduleSessionsComponent
      },
      {
        path: 'start-sessions',
        component: StartSessionsComponent
      },
      {
        path: 'add-course',
        component: AddCourseComponent
      }
      // {
      //   path: 'tc',
      //   component: CourseSelectComponent
      // },
      // {
      //   path: 'error_404',
      //   component: Error404Component
      // },
      // {
      //   path: '**',
      //   redirectTo: 'error_404',
      //   pathMatch: 'full'
      // }
    ]
  }
];

@NgModule({

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MentorRoutingModule { }
