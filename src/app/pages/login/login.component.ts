import { Component, OnInit, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { appVariables } from '../../app-constants';

import { Errors, UserService } from '../../core'

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {

  errors: Errors = { errors: {} };
  isSubmitting = false;
  authForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {
    this.authForm = this.fb.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }

  ngOnInit() {
    $('body').addClass('empty-layout');
  }

  ngAfterViewInit() {
    // if(appVariables.userToken != '') this.router.navigateByUrl('/'+appVariables.currentModule);
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = { errors: {} };

    const credentials = this.authForm.value;
    console.log('credentials: ', credentials);


    this.userService
      .attemptAuth(credentials)
      .subscribe(
        res => {
          console.log('data', res);
          if (res['responsecode'] == 200) {

            appVariables.userToken = res['token'];
            appVariables.userId = res['data']['uId']
            appVariables.userType = res['data']['typeName']

            switch (appVariables.userType) {

              case "UserTypeID-1": {
                appVariables.currentModule = "coach"
                break;
              }
              case "UserTypeID-2": {
                appVariables.currentModule = "trainer"
                break;
              }
              case "UserTypeID-3": {
                appVariables.currentModule = "mentor"
                break;
              }
              case "UserTypeID-4": {
                appVariables.currentModule = "seeker"
                break;
              }
              default: {
                break;
              }
            }
            this.router.navigateByUrl('/' + appVariables.currentModule);
          }
        },
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  }






  ngOnDestroy() {
    $('body').removeClass('empty-layout');
  }

}
