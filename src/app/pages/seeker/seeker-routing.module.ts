import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeekerDashboardComponent } from './seeker-dashboard/seeker-dashboard.component';
// import { CourseComponent } from './course/course.component';
// import { CourseDetailsComponent } from './course-details/course-details.component';
import { UserRatingsComponent } from './user-ratings/user-ratings.component';
// import { ComplimentsComponent } from './compliments/compliments.component';
import { ViewSessionsComponent } from './view-sessions/view-sessions.component';
import { CourseDetailsComponent } from '../course-details/course-details.component';
import { CourseComponent } from '../course/course.component';
import { ComplimentsComponent } from '../compliments/compliments.component';
// import { CourseDetailsComponent } from './course-details/course-details.component';
// import { CourseComponent } from './course/course.component';

const routes: Routes = [
  {
    path: "",
    redirectTo:'dashboard',
    pathMatch:'full'
  },
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: SeekerDashboardComponent
      }
    ]
  },
  {
    path: 'course-details',
    component: CourseDetailsComponent
  },
  {
    path: 'course',
    component: CourseComponent
  },
  {
    path: 'compliments',
    component: ComplimentsComponent
  },
  // {
  //   path: '',
  //   children: [
  //     {
  //       path: 'course',
  //       component: CourseComponent
  //     }
  //   ]
  // },
  // {
  //   path: '',
  //   children: [
  //     {
  //       path: 'course-details',
  //       component: CourseDetailsComponent
  //     }
  //   ]
  // },
  {
    path: '',
    children: [
      {
        path: 'badges',
        component: UserRatingsComponent
      }
    ]
  },
  // {
  //   path: '',
  //   children: [
  //     {
  //       path: 'compliments',
  //       component: ComplimentsComponent
  //     }
  //   ]
  // },
  {
    path: '',
    children: [
      {
        path: 'user-sessions',
        component: ViewSessionsComponent
      }
    ]
  },
  // {
  //   path: '',
  //   children: [
  //     {
  //       path: 'course',
  //       component: CourseComponent
  //     }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeekerRoutingModule { }
