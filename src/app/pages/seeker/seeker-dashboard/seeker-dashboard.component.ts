import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
@Component({
  selector: 'app-seeker-dashboard',
  templateUrl: './seeker-dashboard.component.html',
  styleUrls: ['./seeker-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SeekerDashboardComponent implements AfterViewInit {

  constructor(private _script: ScriptLoaderService) { }

  ngAfterViewInit () {
    this._script.load('./assets/js/scripts/dashboard_visitors.js');
  }

}
