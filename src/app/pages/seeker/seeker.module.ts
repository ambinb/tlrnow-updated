import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeekerRoutingModule } from './seeker-routing.module';
import { SeekerDashboardComponent } from './seeker-dashboard/seeker-dashboard.component';
import { SeekerSidebarComponent } from './_layout/seeker-sidebar/seeker-sidebar.component';
import { SeekerHeaderComponent } from './_layout/seeker-header/seeker-header.component';
// import { CourseComponent } from './course/course.component';
// import { CourseDetailsComponent } from './course-details/course-details.component';
import { UserRatingsComponent } from './user-ratings/user-ratings.component';
// import { ComplimentsComponent } from './compliments/compliments.component';
import { ViewSessionsComponent } from './view-sessions/view-sessions.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
// import { CourseDetailsComponent } from './course-details/course-details.component';
// import { UserRatingsComponent } from './user-ratings/user-ratings.component';
// import { CourseComponent } from './course/course.component';

@NgModule({
  imports: [
    CommonModule,
    SeekerRoutingModule,
    SharedModuleModule
  ],
  exports:[
    SeekerSidebarComponent, SeekerHeaderComponent,
  ],
  declarations: [SeekerDashboardComponent, SeekerSidebarComponent, SeekerHeaderComponent,  UserRatingsComponent, ViewSessionsComponent,
    
    
  ]
})
export class SeekerModule { }
