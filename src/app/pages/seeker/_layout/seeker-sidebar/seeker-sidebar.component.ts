import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-seeker-sidebar]',
  templateUrl: './seeker-sidebar.component.html',
  styleUrls: ['./seeker-sidebar.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class SeekerSidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
