import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-seeker-header]',
  templateUrl: './seeker-header.component.html',
  styleUrls: ['./seeker-header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SeekerHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
