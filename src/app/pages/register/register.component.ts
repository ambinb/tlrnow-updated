import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrComponent } from '../ui/toastr/toastr.component';
declare var $:any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})
// export class RegisterComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
export class RegisterComponent implements OnInit, AfterViewInit, OnDestroy {


  registerForm: FormGroup;

  constructor(private fb: FormBuilder) { 
    this.createForm();
  }


  createForm(){

    this.registerForm = this.fb.group({
      name:['',Validators.required],
      role:'',
      email:['',Validators.email],
      password: ['',Validators.required],
      conPassword:['',Validators.required]
    });
  }

  ngOnInit() {
    $('body').addClass('empty-layout');
  }

  ngAfterViewInit() {
    $('#login-form').validate({
        errorClass:"help-block",
        rules: {
            email: {required:true,email:true},
            password: {required:true},
            password_confirmation: {required:true}
        },
        highlight:function(e){$(e).closest(".form-group").addClass("has-error")},
        unhighlight:function(e){$(e).closest(".form-group").removeClass("has-error")},
    });
  }

  ngOnDestroy() {
    $('body').removeClass('empty-layout');
  }

  onClickRegister(){
    if(this.registerForm.valid){
      
    }else{
      console.log("form in valid");
    }
  }

}

