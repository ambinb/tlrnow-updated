import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appVariables } from '../../app-constants';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class CourseComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  onDetailsClick(){
    console.log(appVariables.currentModule);
    if(appVariables.currentModule != undefined){
      this.router.navigateByUrl(appVariables.currentModule+'/course-details');
    }
    
  }

}
