import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../core';
import { urlsUser } from '../../app-constants';
import { error } from 'util';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
})
export class ForgotPasswordComponent implements OnInit, AfterViewInit, OnDestroy {

  forgotForm: FormGroup;
  constructor(private fb: FormBuilder, 
    private userService: UserService,
    private router: Router) { 
   
    this.createForm();
  }

  createForm(){
    this.forgotForm = this.fb.group({
      email:['',Validators.email]
    });
    console.log("form created");
  }

  ngOnInit() {
    $('body').addClass('empty-layout');
  }

  ngAfterViewInit() {
    $('#forgot-form').validate({
        errorClass: "help-block",
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        highlight: function(e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group").removeClass("has-error")
        },
    });
  }

  ngOnDestroy() {
    $('body').removeClass('empty-layout');
  }

  onSubmit(){

    if(this.forgotForm.valid){
      this.userService.forgotPassword(this.forgotForm.value)
        .subscribe(
          res => {
            console.log(res);
            if(res['statuscode'] == 200){
              this.router.navigateByUrl('/login');
              //toast here the received msg : res.msg
            }
          },
          error => {
            console.error(error);
            
          }
        );
    }
  }

}
