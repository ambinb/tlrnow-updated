import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoachRoutingModule } from './coach-routing.module';
import { CoachSidebarComponent } from './_layout/coach-sidebar/coach-sidebar.component';
import { CoachHeaderComponent } from './_layout/coach-header/coach-header.component';
import { CoachDashboardComponent } from './coach-dashboard/coach-dashboard.component';
// import { CourseComponent } from '../course/course.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    CoachRoutingModule,
    SharedModuleModule
    
  ],
  exports:[
    CoachSidebarComponent,
    CoachHeaderComponent
  ],
  declarations: [
    CoachSidebarComponent,
    CoachHeaderComponent,
    CoachDashboardComponent,
  ]
})
export class CoachModule { }
