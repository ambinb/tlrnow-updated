import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

@Component({
  selector: 'app-coach-dashboard',
  templateUrl: './coach-dashboard.component.html',
  styleUrls: ['./coach-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CoachDashboardComponent implements AfterViewInit {

  constructor(private _script: ScriptLoaderService) { }

  ngAfterViewInit() {
    this._script.load('./assets/js/scripts/dashboard_visitors.js');
  }

}
