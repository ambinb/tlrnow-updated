import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainerRoutingModule } from './trainer-routing.module';
import { TrainerHeaderComponent } from './_layout/trainer-header/trainer-header.component';
import { TrainerSidebarComponent } from './_layout/trainer-sidebar/trainer-sidebar.component';
import { TrainerDashboardComponent } from './trainer-dashboard/trainer-dashboard.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    TrainerRoutingModule,
    SharedModuleModule
  ],
 exports:[
  TrainerHeaderComponent, TrainerSidebarComponent
 ],
  declarations: [TrainerHeaderComponent, TrainerSidebarComponent, TrainerDashboardComponent]
})
export class TrainerModule { }
