import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddCourseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
