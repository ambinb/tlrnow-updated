import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../helpers';

@Component({
  selector: '.page-wrapper',
  templateUrl: './trainer-layout.component.html',
  styleUrls: ['./trainer-layout.component.css']
})
export class TrainerLayoutComponent {

  constructor() { }

  ngAfterViewInit() {

    Helpers.initLayout();
  }

}
