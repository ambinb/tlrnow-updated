import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorsLayoutComponent } from './mentors-layout.component';

describe('MentorsLayoutComponent', () => {
  let component: MentorsLayoutComponent;
  let fixture: ComponentFixture<MentorsLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorsLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
